# ldb.js

[![Build Status](https://travis-ci.org/vinitkumar/ldb.js.svg?branch=master)](https://travis-ci.org/vinitkumar/ldb.js)

HTML5 LocalStorage provides nice key-value storage for data.This small utility abstracts the process and
take care of some edge cases.


```js

var db = new Ldb();

data = {
   "about": "Just Do It.\nwww.nike.com",
   "category": "Product/service",
   "category_list": [
      {
         "id": "2201",
         "name": "Product/Service"
      }
   ],
   "checkins": 631,
   "founded": "1972",
   "general_info": "Nike, Inc.:",
   "is_published": true,
   "location": {
      "street": "1 Bowerman Drive",
      "city": "Beaverton",
      "state": "OR",
      "country": "United States",
      "zip": "97005",
      "latitude": 45.507207,
      "longitude": -122.828402
   },
   "talking_about_count": 52525,
   "username": "nike",
   "website": "http://nike.com",
   "were_here_count": 9133,
   "id": "15087023444",
   "name": "Nike",
   "link": "http://www.facebook.com/nike",
   "likes": 16067637,
   "cover": {
      "cover_id": "10151647887303445",
      "source": "http://sphotos-b.ak.fbcdn.net/hphotos-ak-ash3/s72.png",
      "offset_y": 0,
      "offset_x": 0
   }
};

// First check if your browser has localStorage support
db.check();
// Save the JSON data into localStorage
db.set('key', data);
// Access the data in the application. The following method returns the Object.
db.get('key');
// Remove the data having key as key, checks for keys with no data.
db.remove('key');

```

## Installation and Use:

You can download the latest stable code via bower or npm:
With npm you can download with:

`npm install ldb` or  `bower install ldb.js`

I have also included polyfill.js for enabling localStorage in browsers
where there is no support.

```html
<script type="text/javascript" src="polyfill.js"></script>
<script type="text/javascript" src="ldb.js"></script>
```

## Contributors:

Vinit Kumar [@vinitkumar](http://github.com/vinitkumar)

## Bugs, Issues, Pull Requests


If you have found any bug, kindly report it to issues and if you want to
contribute just fork the repo and get started. Also, it is obvious that
you use the same coding style as the original codebase.







[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/vinitkumar/ldb.js/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

