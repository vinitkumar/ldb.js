describe("Ldb", function () {

  describe("#check", function () {
    beforeEach(function () {
      db = new Ldb();
    });

    it("should return true", function () {
      expect(db.check()).toBe(true);
    });
  });

  describe("#get", function () {
    beforeEach(function () {
      store = {
        users: ["Nithin", "Krishna"]
      };
      spyOn(localStorage, 'getItem').andCallFake(function (key) {
        return JSON.stringify(store[key]);
      });
      db = new Ldb();
    });

    it("should return return data with a given key", function () {
      expect(db.get('users')).toEqual(store.users);
    });
    afterEach(function () {
      localStorage.clear();
    });
  });

  describe("#set", function () {
    beforeEach(function () {
      store = {
        users: ["Nithin", "Krishna"]
      };
      spyOn(localStorage, 'setItem').andCallFake(function (key, value) {
        store[key] = JSON.parse(value);
      });
      db = new Ldb();
    });

    it("should return return data with a given key", function () {
      newUsers = ["Nithin", "Krishna"];
      expect(store.users).toEqual(newUsers);
    });
    afterEach(function () {
      localStorage.clear();
    });
  });

  describe("#remove", function () {
    beforeEach(function () {
      store = {
        users: ["Nithin", "Krishna"]
      };
      spyOn(localStorage, 'removeItem').andCallFake(function (key, value) {
        store[key] = value;
      });
      db = new Ldb();
    });

    it("should not return return data with a given key", function () {
      newUsers = ["Nithin", "Krishna"];
      db.remove('users');
      expect(db.get('users')).toEqual('No such key exists, Are you sure you have saved data with the key?');
    });
    afterEach(function (){
      localStorage.clear();
    });
  });
});