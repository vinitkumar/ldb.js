module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      dist: {
        files: {
          '<%= pkg.name %>.min.js': ['ldb.js']
        }
      }
    },
    jshint: {
      files: ['gruntfile.js', '*.js'],
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    karma: {
      unit: {
        options: {
          configFile: 'karma.conf.js',
          autoWatch: true,
          singleRun: true,

          runnerPort: 9999,
          browsers: ['PhantomJS'],
          logLevel: 'ERROR',

          files: [ 'ldb.js', 'tests/*.js' ],
          tasks: ['karma:unit:run']
        },
        travis: {
          configFile: 'karma.conf.js',
          singleRun: true,
          browsers: ['PhantomJS']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-karma');

  grunt.registerTask('default', ['jshint', 'uglify']);
  grunt.registerTask('test', ['karma:unit']);

};
