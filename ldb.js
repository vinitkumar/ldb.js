// Copyright &copy; 2014  [Vinit Kumar](mailto:vinit.kumar@changer.nl)
// 
// MIT Licensed
 
var Ldb = function () {};


Ldb.prototype.check = function () {
  try {
    return 'localStorage' in window && window.localStorage !== null;
  } catch (e) {
    return false;
  }
};


Ldb.prototype.get = function (key) {
  if (localStorage.getItem(key)) {
    return JSON.parse(localStorage.getItem(key));
  } else {
    return 'No such key exists, Are you sure you have saved data with the key?';
  }
};

Ldb.prototype.set = function(key, value) {
  if (typeof (value) === 'object') {
    var stringValue = JSON.stringify(value);
    localStorage.setItem(key, stringValue);
    return 'Successfully saved your data to the key' + key +' key!';
  }
};


Ldb.prototype.remove = function(key) {
  if (localStorage.getItem(key)) {
    localStorage.removeItem(key);
    return 'Successfully removed data with the key'+ key +' !';
  } else {
    return 'No such key exists, nothing to remove';
  }
};
